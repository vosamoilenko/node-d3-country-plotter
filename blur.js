const Jimp = require("jimp")

async function blur(filepath) {
  const parts = filepath.split(".png")

  // Function name is same as of file name
  // Reading Image
  const image = await Jimp.read("./coordinates.png")
  image.blur(3).write(`${parts[0]}_blurred.png`)
}

blur(process.argv[2]) // Calling the function here using async
console.log("Image is processed successfully")
