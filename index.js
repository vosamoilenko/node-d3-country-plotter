const d3 = require("d3")
const jsdom = require("jsdom")
const fs = require("fs")
const svg2png = require("svg2png")
const { JSDOM } = jsdom

class JsDom {
  constructor() {
    this.initJSDOM()
  }

  initJSDOM() {
    try {
      const jsDom = new JSDOM("")
      const window = jsDom.window
      global.window = window

      const { document } = window
      global.document = document

      window.innerWidth = window.outerWidth = 1454
      window.innerHeight = window.outerHeight = 1040
    } catch (e) {
      console.error("Cannot initialize JSDOM" + e.message)
    }
  }

  getSvgFromElement() {
    return document.querySelector("svg").outerHTML
  }
}

function start() {
  const mockedDom = new JsDom()
  const argv = process.argv.splice(2)

  const jsonFileWithCoordinatesPath = argv[0]
  const outputfilename = jsonFileWithCoordinatesPath.replace("json", "png")

  const width = window.innerWidth
  const height = window.innerHeight
  const projection = d3.geoMercator().scale(3000).translate([-1130, 3450])

  const coordinatesRaw = fs.readFileSync(jsonFileWithCoordinatesPath, {
    encoding: "utf8",
  })
  const coordinatesItems = JSON.parse(coordinatesRaw)
  const coordinates = coordinatesItems.map((line) => line.split(","))

  const svg = d3
    .select("body")
    .append("svg")
    .attr("width", width)
    .attr("height", height)

  svg
    .selectAll("circle")
    .data(coordinates)
    .enter()
    .append("circle")
    .attr("cx", function (point) {
      return projection(point)[0]
    })
    .attr("cy", function (point) {
      return projection(point)[1]
    })
    .attr("r", function () {
      return "3px"
    })
    .attr("fill", function () {
      return "black"
    })

  const svgEl = mockedDom.getSvgFromElement()
  const png = svg2png.sync(svgEl, { width: 1000, height: 1000 })
  console.log(outputfilename)
  fs.writeFileSync(outputfilename, png)
}

start()
